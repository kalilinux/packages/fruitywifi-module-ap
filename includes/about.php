<b>Access Point</b> module by @xtr4nge
<br><br>
- Filters have been added (MACs and SSID)
<br>
- Connected Stations are displayed on Clients Tab.
<br>
- DNS Server can be changed from DHCP-DNS Tab [DNSmasq or FruityDNS]

<br><br>

<b>Picker</b>: Collects all the SSIDs around you.
<br>
<b>Scatter</b>: Broadcast all the SSIDs collected.
<br>
<b>Polite</b>: Response to all the probe requests.